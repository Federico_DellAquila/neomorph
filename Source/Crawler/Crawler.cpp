// Copyright Epic Games, Inc. All Rights Reserved.

#include "Crawler.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Crawler, "Crawler" );
