// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CrawlerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CRAWLER_API ACrawlerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
