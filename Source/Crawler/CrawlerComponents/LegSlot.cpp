// Project includes
#include "Crawler/CrawlerComponents/LegSlot.h"
#include "Crawler/CrawlerComponents/Leg.h"

// Engine includes
#include "Kismet/KismetMathLibrary.h"
#include "Math/NumericLimits.h"

// Sets default values
ALegSlot::ALegSlot()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void ALegSlot::BeginPlay()
{
    Super::BeginPlay();

    // Attach the LegSlot to its relative Associated Leg Slot Location received from Body on Spawn
    if (ensure(AssociatedLegSlotLocation))
    {
        FAttachmentTransformRules TransformRules = FAttachmentTransformRules(EAttachmentRule::SnapToTarget,
            EAttachmentRule::KeepRelative, EAttachmentRule::KeepWorld, true);
        AttachToComponent(AssociatedLegSlotLocation, TransformRules);
    }
}

void ALegSlot::Trace(FVector& HitLocation, FVector& HitNormal, bool& bHit, 
    const EDrawDebugTrace::Type DrawDebugType /*= EDrawDebugTrace::Type::None*/, const float DrawTime /*= 1.0f*/)
{
    UWorld* World = GetWorld();
    if (ensure(World))
    {
        FHitResult HitResult;
        const FVector Start = GetActorLocation();
        const FVector End = Start + GetActorUpVector() * -10000.0f;

        UKismetSystemLibrary::LineTraceSingle(World, Start, End, UEngineTypes::ConvertToTraceType(ECC_Visibility), 
            true, TArray<AActor*>(), DrawDebugType, HitResult, true, FLinearColor::Red, FLinearColor::Green, DrawTime);

        HitLocation = HitResult.Location;
        HitNormal = HitResult.Normal;
        bHit = HitResult.bBlockingHit;
    }
}

bool ALegSlot::ShouldLegMove(const FVector TracedLocation) const
{
    if (ensure(AssociatedLeg)) 
    {
        // We used DistSquared and Pow to avoid calculating an unnecessary Square Root
        const float LegToTracedDistance = FVector::DistSquared(AssociatedLeg->GetActorLocation(), TracedLocation);
        const float SquaredLegThreshold = FMath::Pow(AssociatedLeg->Threshold, 2);
        return LegToTracedDistance > SquaredLegThreshold;
    }
    return false;
}

FVector ALegSlot::CalculateNewTraceDirection(const FVector TracedLocation, const float Distance) const
{
    if (ensure(AssociatedLeg))
    {
        const FVector LegToTracedDirection = (TracedLocation - AssociatedLeg->GetActorLocation()).GetSafeNormal();
        const FVector LegToTracedDistanceVector = LegToTracedDirection * Distance;
        const FVector DistanceVectorFromTraced = TracedLocation + LegToTracedDistanceVector;
        const FVector NewTraceDirection = (DistanceVectorFromTraced - GetActorLocation()).GetSafeNormal();
        return NewTraceDirection;
    }
    return FVector(0.0f, 0.0f, 0.0f);
}

float ALegSlot::CalculateLegMovementDistance(const float AdjustValue /*= 0.5f*/) const
{
    if (ensure(AssociatedLeg)) 
    {
        // To keep it consistent, we use the Min and Max Threshold to calculate the Distance value
        const float Min = AssociatedLeg->MinThreshold;
        const float Max = AssociatedLeg->MaxThreshold;
        const float Distance = FMath::RandRange(Min, Max) * AdjustValue;
        return Distance;
    }
    return 0.0f;
}

void ALegSlot::CalculateNewLegLocationAndRotation(const FVector TracedLocation,
    const EDrawDebugTrace::Type DrawDebugType /*= EDrawDebugTrace::Type::None*/, const float DrawTime /*= 0.25f*/) const
{
    UWorld* World = GetWorld();
    if (ensure(World) && ensure(AssociatedLeg))
    {
        // We calculate an offset for the final location from Traced Location
        const float Distance = CalculateLegMovementDistance();
        // Direction from LegSlot location to the Offset location
        const FVector NewDirection = CalculateNewTraceDirection(TracedLocation, Distance);

        FHitResult HitResult;
        const FVector Start = GetActorLocation();
        const FVector End = Start + NewDirection * 10000.0f;
        // We want to ignore the Body
        const TArray<AActor*> ActorsToIgnore{ (AActor*)AssociatedBody };

        UKismetSystemLibrary::LineTraceSingle(World, Start, End, UEngineTypes::ConvertToTraceType(ECC_Visibility),
            true, TArray<AActor*>(), DrawDebugType, HitResult, true, FLinearColor(FColor::Magenta), FLinearColor::Green, DrawTime);

        // We calculate final Position and Orientation
        const FVector NewPosition = HitResult.Location;
        const FRotator NewOrientation = UKismetMathLibrary::FindLookAtRotation(NewPosition, NewPosition + HitResult.Normal);

        // We set the Associated Leg Position and Orientation
        AssociatedLeg->SetPositionAndOrientation(NewPosition, NewOrientation);
    }
}