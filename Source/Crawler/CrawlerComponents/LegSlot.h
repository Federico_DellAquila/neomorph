#pragma once
#include "CoreMinimal.h"

// Engine includes
#include "Kismet/KismetSystemLibrary.h"

#include "GameFramework/Actor.h"
#include "LegSlot.generated.h"


class ABody;
class ALeg;

UCLASS()
class CRAWLER_API ALegSlot : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALegSlot();

	/** Body Associated to this LegSlot */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Body")
	ABody* AssociatedBody;
	/** Leg Associated to this LegSlot */
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Leg")
    ALeg* AssociatedLeg;
	/** Used as reference point to attach LegSlot to Body */
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "LegSlotLocation")
    USceneComponent* AssociatedLegSlotLocation;

	/** Trace a line straight down relative to the Up Vector of LegSlot and return Hit Location and Normal */
	UFUNCTION(BlueprintCallable, Category = "Tracing")
	virtual void Trace(FVector& HitLocation, FVector& HitNormal, bool& bHit, 
        const EDrawDebugTrace::Type DrawDebugType = EDrawDebugTrace::Type::None, const float DrawTime = 1.0f);

	/** Check if the distance between Traced Location and Leg is greater than Leg Threshold */
	UFUNCTION(BlueprintCallable, Category = "Tracing")
	virtual bool ShouldLegMove(const FVector TracedLocation) const;
	/** Calculate a new Location and Rotation for the Leg considering a Distance offset from the Traced Location */
    UFUNCTION(BlueprintCallable, Category = "Tracing")
    void CalculateNewLegLocationAndRotation(const FVector TracedLocation,
		const EDrawDebugTrace::Type DrawDebugType = EDrawDebugTrace::Type::None, const float DrawTime = 0.25f) const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** Calculate a new Trace Direction relative to the Distance value from the Traced Location */
	UFUNCTION(BlueprintCallable, Category = "Tracing")
	FVector CalculateNewTraceDirection(const FVector TracedLocation, const float Distance) const;
	/** Calculate a random Distance to be used as an offset from the Traced Location 
		to calculate a new Leg Location */
    UFUNCTION(BlueprintCallable, Category = "Tracing")
    float CalculateLegMovementDistance(const float AdjustValue =  0.5f) const;
};
