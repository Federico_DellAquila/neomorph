#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Body.generated.h"

// Forward declarations
class UFloatingPawnMovement;
class UBoxComponent;
class ALegSlot;
class ALeg;

UCLASS()
class CRAWLER_API ABody : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABody();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Body")
	UBoxComponent* BoxCollider;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Body")
	UStaticMeshComponent* BodyMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Legs")
	TSubclassOf<ALeg> LegClass;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LegSlots")
    TSubclassOf<ALegSlot> LegSlotClass;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "LegSlots")
    TArray<ALegSlot*> LegSlotsList;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LegSlots")
	/** Used to keep all Leg Slots Locations at the same height */
	USceneComponent* LegSlotsLocationsPivot;
	/** Front Left Leg Slot Location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LegSlots")
	USceneComponent* FL_LegSlotLocation;
	/** Front Right Leg Slot Location */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LegSlots")
    USceneComponent* FR_LegSlotLocation;
	/** Back Left Leg Slot Location */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LegSlots")
    USceneComponent* BL_LegSlotLocation;
	/** Back Right Leg Slot Location */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LegSlots")
    USceneComponent* BR_LegSlotLocation;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/** Spawn all the necessary LegSlots and Legs */
	UFUNCTION(BlueprintCallable, Category = "Legs")
	virtual void SpawnLegs();
};
