// Projects includes
#include "Crawler/CrawlerComponents/Body.h"
#include "Crawler/CrawlerComponents/LegSlot.h"
#include "Crawler/CrawlerComponents/Leg.h"

// Engine includes
#include "Kismet/GameplayStatics.h"
#include "Components/BoxComponent.h"

// Sets default values
ABody::ABody()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
	BoxCollider->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BoxCollider->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	BoxCollider->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Ignore);
	BoxCollider->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	RootComponent = BoxCollider;

	BodyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BodyMesh"));
	BodyMesh->SetupAttachment(BoxCollider);

	LegSlotsLocationsPivot = CreateDefaultSubobject<USceneComponent>(TEXT("LegSlotsLocationsPivot"));
	LegSlotsLocationsPivot->SetupAttachment(BodyMesh);

    FL_LegSlotLocation = CreateDefaultSubobject<USceneComponent>(TEXT("FL_LegSlotLocation"));
	FL_LegSlotLocation->SetupAttachment(LegSlotsLocationsPivot);
    FR_LegSlotLocation = CreateDefaultSubobject<USceneComponent>(TEXT("FR_LegSlotLocation"));
    FR_LegSlotLocation->SetupAttachment(LegSlotsLocationsPivot);
    BL_LegSlotLocation = CreateDefaultSubobject<USceneComponent>(TEXT("BL_LegSlotLocation"));
    BL_LegSlotLocation->SetupAttachment(LegSlotsLocationsPivot);
    BR_LegSlotLocation = CreateDefaultSubobject<USceneComponent>(TEXT("BR_LegSlotLocation"));
    BR_LegSlotLocation->SetupAttachment(LegSlotsLocationsPivot);
}

// Called when the game starts or when spawned
void ABody::BeginPlay()
{
	Super::BeginPlay();
	
	SpawnLegs();
	
	for (ALegSlot* LegSlot : LegSlotsList)
	{
		FVector HitLocation = FVector();
		FVector HitNormal = FVector();
		bool bHit = false;
		LegSlot->Trace(HitLocation, HitNormal, bHit);
		LegSlot->CalculateNewLegLocationAndRotation(HitLocation);
	}
}

// Called every frame
void ABody::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	for (ALegSlot* LegSlot : LegSlotsList) 
	{
        FVector HitLocation = FVector();
        FVector HitNormal = FVector();
        bool bHit = false;
		LegSlot->Trace(HitLocation, HitNormal, bHit, EDrawDebugTrace::ForOneFrame);
		if (LegSlot->ShouldLegMove(HitLocation)) 
		{			
			LegSlot->CalculateNewLegLocationAndRotation(HitLocation, EDrawDebugTrace::ForDuration);
		}
	}
}

void ABody::SpawnLegs()
{
	UWorld* World = GetWorld();
	if (ensure(World)) 
	{
		const TArray<USceneComponent*> LegSlotsLocationsList 
		{ FL_LegSlotLocation, FR_LegSlotLocation, BL_LegSlotLocation, BR_LegSlotLocation };

		for (USceneComponent* LegSlotLocation : LegSlotsLocationsList)
		{
			if (ensure(LegSlotLocation) && ensure(LegSlotClass))
			{
				// We use 'SpawnActorDeferred' to allow us to initialize some variables before spawing the Actor
                const FTransform LegSlotSpawnTransform(GetActorRotation(), GetActorLocation());
				ALegSlot* LegSlot = World->SpawnActorDeferred<ALegSlot>(LegSlotClass, LegSlotSpawnTransform, this);
				if (ensure(LegSlot)) 
				{
					LegSlot->AssociatedBody = this;
					LegSlot->AssociatedLegSlotLocation = LegSlotLocation;
					UGameplayStatics::FinishSpawningActor(LegSlot, LegSlotSpawnTransform);
					LegSlotsList.AddUnique(LegSlot);

					if (ensure(LegClass)) 
					{
                        const FTransform LegSpawnTransform(LegSlot->GetActorRotation(), LegSlot->GetActorLocation());
                        ALeg* Leg = World->SpawnActorDeferred<ALeg>(LegClass, LegSpawnTransform, this);
                        if (ensure(Leg))
                        {
                            Leg->AssociatedLegSlot = LegSlot;
                            UGameplayStatics::FinishSpawningActor(Leg, LegSpawnTransform);
                        }
					}
				}
			}
		}
	}
}
