// Project includes
#include "Crawler/CrawlerComponents/Leg.h"
#include "Crawler/CrawlerComponents/LegSlot.h"

// Engine includes
#include "CableComponent.h" // Need to include the 'CableComponent' module

// Sets default values
ALeg::ALeg()
	: Threshold(0.0f)
	, MinThreshold(100.0f)
	, MaxThreshold(180.0f)
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = false;

	LegMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("LegMesh"));

	Cable = CreateDefaultSubobject<UCableComponent>(TEXT("Cable"));
	// We want to keep NumSegments to 1 to simulate some kind of leg
	Cable->NumSegments = 1;
	// We don't want any offset to the End Location
	Cable->EndLocation = FVector(0.0f, 0.0f, 0.0f);
	Cable->SetupAttachment(LegMesh);
}

// Called when the game starts or when spawned
void ALeg::BeginPlay()
{
    Super::BeginPlay();

    // Attach the Cable End to the Associated LegSlot
    if (ensure(AssociatedLegSlot))
    {
        Cable->SetAttachEndTo(AssociatedLegSlot, NAME_None);
        AssociatedLegSlot->AssociatedLeg = this;
    }
}

void ALeg::SetPositionAndOrientation(const FVector NewPosition, const FRotator NewOrientation)
{
	SetActorLocationAndRotation(NewPosition, NewOrientation);
	RandomizeThreshold();
}

void ALeg::RandomizeThreshold()
{
	Threshold = FMath::RandRange(MinThreshold, MaxThreshold);
}