#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Leg.generated.h"

// Forward declarations
class UCableComponent;
class ALegSlot;

UCLASS()
class CRAWLER_API ALeg : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALeg();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "LegSlot")
	ALegSlot* AssociatedLegSlot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Leg")
	UStaticMeshComponent* LegMesh;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Leg")
    UCableComponent* Cable;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Leg")
	float Threshold;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Leg")
    float MinThreshold;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Leg")
    float MaxThreshold;

	UFUNCTION(BlueprintCallable, Category = "Leg")
	void SetPositionAndOrientation(const FVector NewPosition, const FRotator NewOrientation);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** Calculate a new random value for Threshold */
	UFUNCTION(BlueprintCallable, Category = "Leg")
	void RandomizeThreshold();
};
