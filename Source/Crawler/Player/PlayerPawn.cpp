// Project includes
#include "Crawler/Player/PlayerPawn.h"

// Engine includes
#include "GameFramework/FloatingPawnMovement.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

APlayerPawn::APlayerPawn()
    : Super()
    , Speed(30.0f)
    , UpSpeedAdjustment(15.0f)
{
    // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    bUseControllerRotationYaw = true;

    // We use a UFloatingPawnMovement to enable simple movements
    PawnMovements = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("PawnMovements"));

    SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
    // We want the Spring Arm to rotate by following the Controller rotation
    SpringArm->bUsePawnControlRotation = true;
    SpringArm->TargetArmLength = 400.0f;
    SpringArm->SetupAttachment(RootComponent);

    PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
    PawnCamera->SetupAttachment(SpringArm);
}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    // We bind some functions to Keyboard and Mouse Axis
    PlayerInputComponent->BindAxis("TurnUp", this, &APlayerPawn::TurnUp);
    PlayerInputComponent->BindAxis("TurnRight", this, &APlayerPawn::TurnRight);
    PlayerInputComponent->BindAxis("MoveForward", this, &APlayerPawn::MoveForward);
    PlayerInputComponent->BindAxis("MoveRight", this, &APlayerPawn::MoveRight);
    PlayerInputComponent->BindAxis("MouseWheel", this, &APlayerPawn::MoveUp);
}

void APlayerPawn::TurnUp(const float AxisValue)
{
    UWorld* World = GetWorld();
    if (ensure(World))
    {
        APlayerController* PC = UGameplayStatics::GetPlayerController(World, 0);
        PC->AddPitchInput(AxisValue);
    }
}

void APlayerPawn::TurnRight(const float AxisValue)
{
    UWorld* World = GetWorld();
    if (ensure(World))
    {
        APlayerController* PC = UGameplayStatics::GetPlayerController(World, 0);
        PC->AddYawInput(AxisValue);
    }
}

void APlayerPawn::MoveForward(const float AxisValue)
{
    AxisValues.X = AxisValue;
}

void APlayerPawn::MoveRight(const float AxisValue)
{
    AxisValues.Y = AxisValue;
}

void APlayerPawn::MoveUp(const float AxisValue)
{
    UWorld* World = GetWorld();
    if (ensure(World))
    {
        const FVector UpDirection = AxisValue * GetActorUpVector();
        AddActorWorldOffset(UpDirection * Speed * UpSpeedAdjustment * World->GetDeltaSeconds(), true);
    }
}

void APlayerPawn::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    if (AxisValues.X != 0.0f || AxisValues.Y != 0.0f)
    {
        /** We combine the movement axis into one and we normalize
            it to avoid having increased diagonal speed */
        const FVector ForwardMovementsVector = GetActorForwardVector() * AxisValues.X;
        const FVector RightMovementsVector = GetActorRightVector() * AxisValues.Y;
        const FVector MovementDirection = (ForwardMovementsVector + RightMovementsVector).GetSafeNormal();

        AddMovementInput(MovementDirection, Speed * DeltaSeconds);
    }
}