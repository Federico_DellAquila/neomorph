#pragma once
#include "CoreMinimal.h"
#include "Crawler/CrawlerComponents/Body.h"
#include "PlayerPawn.generated.h"

// Forward declarations
class UFloatingPawnMovement;
class USpringArmComponent;
class UCameraComponent;

UCLASS()
class CRAWLER_API APlayerPawn : public ABody
{
	GENERATED_BODY()
	
public:
	APlayerPawn();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movements")
	UFloatingPawnMovement* PawnMovements;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movements")
    float Speed;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movements")
    float UpSpeedAdjustment;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
    USpringArmComponent* SpringArm;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
    UCameraComponent* PawnCamera;

    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movements")
    FVector AxisValues;

    UFUNCTION(BlueprintCallable, Category = "Camera")
    void TurnUp(const float AxisValue);
    UFUNCTION(BlueprintCallable, Category = "Camera")
    void TurnRight(const float AxisValue);
    UFUNCTION(BlueprintCallable, Category = "Movement")
    void MoveForward(const float AxisValue);
    UFUNCTION(BlueprintCallable, Category = "Movement")
    void MoveRight(const float AxisValue);
    UFUNCTION(BlueprintCallable, Category = "Movement")
    void MoveUp(const float AxisValue);

    virtual void Tick(float DeltaSeconds) override;
};
